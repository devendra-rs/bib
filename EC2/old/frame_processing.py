import os
import time
import boto3
import pickle
import random
import numpy as np
import pandas as pd
import librosa
from glob import glob
from itertools import cycle
from tqdm import tqdm
from scipy.fft import fft, fftfreq
from scipy import signal
from sklearn.svm import SVR
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from scipy.fft import fft, fftfreq
from scipy import signal

import threading
import concurrent.futures

sr: int = 44100


frame_time = 800
frame_size = int(sr * frame_time / 1000)

overlap_per = 50
overlap_time = frame_time * overlap_per / 100
overlap = int(frame_size * overlap_per / 100)

def process_frame(start, y, file_to_download, sr):
    time_frame_data_maxs = []
    time_frame_data_mins = []
    time_frame_data_means = []
    time_frame_data_medians = []
    time_frame_data_stds = []
    time_frame_data_ptps = []

    amplitude_evelopes = []
    root_mean_square_energies = []
    zero_crossing_rates = []

    freq_magnitude_prod_frame_data_mins = []
    freq_magnitude_prod_frame_data_maxs = []
    freq_magnitude_prod_frame_data_means = []
    freq_magnitude_prod_frame_data_medians = []
    freq_magnitude_prod_frame_data_stds = []
    freq_magnitude_prod_frame_data_ptps = []

    split_frequency = 1500
    band_energy_ratios = []
    spectral_centroids = []
    bandwidths = []

    gulp_constituencies = []

    audio_name = []

    stop = (start + frame_size) if (start + frame_size) < len(y) else (len(y) - 1)
    time_frame_data = y[start:stop]
    if (stop - start) >= overlap:
        # Time based Statistical Features
        time_frame_data_mins.append(np.amin(time_frame_data))
        time_frame_data_maxs.append(np.amax(time_frame_data))
        time_frame_data_means.append(np.mean(time_frame_data))
        time_frame_data_medians.append(np.median(time_frame_data))
        time_frame_data_stds.append(np.std(time_frame_data))
        time_frame_data_ptps.append(np.ptp(time_frame_data))

        # Time domain features
        amplitude_evelopes.append(np.amax(time_frame_data))
        root_mean_square_energies.append(np.sqrt(np.mean(time_frame_data**2)))
        zero_crossing_rates.append(0.5 * np.sum([np.abs(np.sign(time_frame_data[idx]) - np.sign(time_frame_data[idx + 1])) for idx in range(len(time_frame_data) - 1)]))

        # Fourier transform with hann windowing
        N = (len(time_frame_data))
        yf = np.abs(np.fft.fft(signal.hann(N) * time_frame_data)) [:N//2]
        xf = np.linspace(0, sr, N)[:N//2]

        # Frequency based Statistical Features
        freq_magnitude_prod_frame_data = xf * yf
        freq_magnitude_prod_frame_data_mins.append(np.amin(freq_magnitude_prod_frame_data))
        freq_magnitude_prod_frame_data_maxs.append(np.amax(freq_magnitude_prod_frame_data))
        freq_magnitude_prod_frame_data_means.append(np.mean(freq_magnitude_prod_frame_data))
        freq_magnitude_prod_frame_data_medians.append(np.median(freq_magnitude_prod_frame_data))
        freq_magnitude_prod_frame_data_stds.append(np.std(freq_magnitude_prod_frame_data))
        freq_magnitude_prod_frame_data_ptps.append(np.ptp(freq_magnitude_prod_frame_data))

        # Frequency domain features
        temp = np.sum(yf[int(split_frequency* (N / sr)):] ** 2)
        if temp == 0 or np.sum(yf) == 0:
            band_energy_ratios.append(0)
            spectral_centroids.append(0)
            bandwidths.append(0)
        else:
            band_energy_ratios.append(np.sum(yf[:int(split_frequency* (N / sr))] ** 2) / temp)
            spectral_centroids.append(np.sum(yf * xf) / np.sum(yf))
            bandwidths.append(np.sum((xf - (np.sum(yf * xf) / np.sum(yf))) * yf) / np.sum(yf))

        # Gulp Data

        # Audio Metadata
        audio_name.append(file_to_download)
    return {
        'time_frame_data_mins': time_frame_data_mins,
        'time_frame_data_maxs': time_frame_data_maxs,
        'time_frame_data_means': time_frame_data_means,
        'time_frame_data_medians': time_frame_data_medians,
        'time_frame_data_stds': time_frame_data_stds,
        'time_frame_data_ptps': time_frame_data_ptps,
        'amplitude_evelopes': amplitude_evelopes,
        'root_mean_square_energies': root_mean_square_energies,
        'zero_crossing_rates': zero_crossing_rates,
        'freq_magnitude_prod_frame_data_mins': freq_magnitude_prod_frame_data_mins,
        'freq_magnitude_prod_frame_data_maxs': freq_magnitude_prod_frame_data_maxs,
        'freq_magnitude_prod_frame_data_means': freq_magnitude_prod_frame_data_means,
        'freq_magnitude_prod_frame_data_medians': freq_magnitude_prod_frame_data_medians,
        'freq_magnitude_prod_frame_data_stds': freq_magnitude_prod_frame_data_stds,
        'freq_magnitude_prod_frame_data_ptps': freq_magnitude_prod_frame_data_ptps,
        'band_energy_ratios': band_energy_ratios,
        'spectral_centroids': spectral_centroids,
        'bandwidths': bandwidths,
        'audio_name': audio_name,
        }

