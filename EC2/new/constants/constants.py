sr: int = 44100

# 800ms frame time with 50% overlap

frame_time: int = 800
frame_size: int = int(sr * frame_time / 1000)

overlap_per: int = 50
overlap_time: float = frame_time * overlap_per / 100
overlap: int = int(frame_size * overlap_per / 100)
