import pickle
import threading
import numpy as np
import pandas as pd
import concurrent.futures
from sklearn.svm import SVR
from sklearn.pipeline import make_pipeline
from joblib import Parallel, delayed
from sklearn.preprocessing import StandardScaler

from utils.mail_util import send_mail
from utils.accelerometer_data_loading import binary_data_loading
from utils.num_to_sec import num_to_sec
from utils.gulp_to_volume import gulp_to_volume
from utils.get_num_gulps import get_num_gulps

from constants.constants import *

from frame_processing import process_frame

def process_audio(file_to_download, audio, sr):
    print(f"Process: {file_to_download}")

    audio_name = []

    y = audio

    # Process individual frames
    results = Parallel(n_jobs=-1)(
            delayed(process_frame)(start, y, file_to_download, sr) for start in range(0, len(y), overlap)
        )

    combined_results = {
        'time_frame_data_mins': [],
        'time_frame_data_maxs': [],
        'time_frame_data_means': [],
        'time_frame_data_medians': [],
        'time_frame_data_stds': [],
        'time_frame_data_ptps': [],
        'amplitude_evelopes': [],
        'root_mean_square_energies': [],
        'zero_crossing_rates': [],
        'freq_magnitude_prod_frame_data_mins': [],
        'freq_magnitude_prod_frame_data_maxs': [],
        'freq_magnitude_prod_frame_data_means': [],
        'freq_magnitude_prod_frame_data_medians': [],
        'freq_magnitude_prod_frame_data_stds': [],
        'freq_magnitude_prod_frame_data_ptps': [],
        'band_energy_ratios': [],
        'spectral_centroids': [],
        'bandwidths': [],
        'audio_name': [],
        }

    # cumulate individual frame data
    for result in results:
        for key in result.keys():
            if len(result[key]) == 0: continue
            combined_results[key].extend(result[key])


    gulp_analysis = pd.DataFrame()

    # Store in a dataframe
    for key in combined_results.keys():
        gulp_analysis[key] = combined_results[key]

    print(gulp_analysis.head())

    # Make feature vector
    x = gulp_analysis.drop(['audio_name'], axis=1).dropna().values

    # Load classifier
    clf = pickle.load(open('gulp_detection_model_2.sav', 'rb'))
    print(f"Classifier loaded: {sum(clf.predict(x))}")

    # Get number of gulps
    num_of_gulps: int = get_num_gulps(sum(clf.predict(x)))
    print(f"Num of gulps: {num_of_gulps}")

    # Get volume of liquid consumed
    volume_of_liquid_consumed: float = gulp_to_volume(num_of_gulps)

    print(f"Volume of liquid: {volume_of_liquid_consumed}")

    # Return result
    return {"Volume": (round(volume_of_liquid_consumed, 2)), "Gulps": num_of_gulps}
