import numpy as np

# Normalize number of gulps
def get_num_gulps(num: float) -> int:
    return np.round(num * 0.01)
