import base64
import sendgrid
from sendgrid.helpers.mail import *

# Define key
sg = sendgrid.SendGridAPIClient(api_key='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')

def send_mail(content_text='', subject='', images_to_add=[]):
    # Define sender
    from_email = Email("accounts@remotesymphony.com")

    # Define to send
    to_emails = [
            To('kapil.chauhan@remotesymphony.com', 'Kapil Chauhan', p=1),
            To('sameer.khadilkar@remotesymphony.com', 'Sameer Khadilkar', p=1),
            To('ankur.patil@remotesymphony.com', 'Ankur Patil', p=1),
            To('aayush.rai@remotesymphony.com', 'Aayush Rai', p=1),
            To('akhila.kala@gadgeon.com', 'Akhila Kala', p=1),
        ]

    # Define content
    content = Content("text/plain", content_text)

    mail = Mail(from_email, to_emails, subject, content)

    # Attach images
    for image in images_to_add:
        with open(image, 'rb') as f:
            mail.attachment = sendgrid.Attachment(
                disposition='inline',
                file_name='accelerometer_'+image,
                file_type='image/png',
                file_content=base64.b64encode(f.read()).decode(),
                content_id='accelerometer_graph',
            )

    try:
        # Send mail
        response = sg.client.mail.send.post(request_body=mail.get())
        print(response.status_code)
        print(response.body)
        print(response.headers)
        return True
    except Exception as e:
        print(str(e))
        return False

