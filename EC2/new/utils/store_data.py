import pymysql

# Establish connection
username="admin"
password="VQ72G8h*PFfz"
database="bibdb"
host="database-bib.citegk9b5uwi.us-east-1.rds.amazonaws.com"
conn = pymysql.connect(
        host=host,
        user=username,
        password = password,
        db=database,
        autocommit=True
       )

def store_data(key='', result={}):
    # define cursor
    cur = conn.cursor()
    
    # Get id of the session
    print(cur.execute(f"SELECT `id` FROM sessionRecordingFiles WHERE `fileName` = '{key}'"))
    
    id_ = cur.fetchall()[0][0]
    print(id_)

    # Get physilogical data
    print(cur.execute(f"SELECT `sessionMasterId`, `infantWeight`, `genderOfInfant`, `ageOfInfant` FROM sessionPhysiologicalData WHERE `sessionMasterId` = {id_}"))
    out = cur.fetchall()[0]
    
    age = out[3]
    gender = out[2]
    weight = out[1]
    number_of_gulps = result['Gulps']
    
    total_milk_consumed = result['Volume']
    
    id_ = int(key.split('_')[-3])
    public = number_of_gulps * 12
    
    # Update session masters table
    cur.execute(f"UPDATE sessionMasters set `AmtOfMilk_Estimated` = {total_milk_consumed} where `id`={id_}")
    conn.commit()
    cur.execute(f"UPDATE sessionMasters set `numberOfGulps` = {number_of_gulps} where `id`={id_}")
    conn.commit()
    cur.execute(f"UPDATE sessionMasters set `AmtOfMilk_OurDataCollection` = 200 where `id`={id_}")
    conn.commit()
    cur.execute(f"UPDATE sessionMasters set `AmtOfMilk_PublicReferences` = {public} where `id`={id_}")
    conn.commit()
    cur.execute(f"UPDATE sessionMasters set `ConfidenceRating` = 82 where `id`={id_}")
    conn.commit()
    cur.close()

    return id_
