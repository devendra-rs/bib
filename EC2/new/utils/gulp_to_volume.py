# Convert gulp to volume randomly (No regression data available

import random
def gulp_to_volume(num_gupls: int) -> float:
    return (random.randint(25, 35) * num_gupls);
