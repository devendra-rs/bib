from .twos_complement import twos_complement

# Loading accelerometer data
def binary_data_loading(file: str):
    if file == '': return []
    hex_data = open(file, 'rb').read().hex()
    final_data = []
    for base_idx in range(0, len(hex_data), 28):
        row = {
                'hex_str_sec': '',
                'second_epoch_int': 0,
                'second_epoch_twos_complement': 0,
                'hex_str_ms' : '',
                'ms_epoch_int': 0,
                'ms_epoch_twos_complement': 0,
                'x_val': 0,
                'y_val': 0,
                'z_val': 0
                }

        idx = base_idx
        row['hex_str_sec'] = hex_data[idx + 6: idx + 8] + hex_data[idx + 4: idx + 6] + hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]
        row['second_epoch_int'] = int((hex_data[idx + 6: idx + 8] + hex_data[idx + 4: idx + 6] + 
                                hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]), 16)
        row['second_epoch_twos_complement'] = twos_complement((hex_data[idx + 6: idx + 8] + hex_data[idx + 4: idx + 6] + 
                                hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]), 32)

        idx += 8
        row['hex_str_ms'] = hex_data[idx + 6: idx + 8] + hex_data[idx + 4: idx + 6] + hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]
        row['ms_epoch_int'] = int((hex_data[idx + 6: idx + 8] + hex_data[idx + 4: idx + 6] + 
                                hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]), 16)
        row['ms_epoch_twos_complement'] = twos_complement((hex_data[idx + 6: idx + 8] + hex_data[idx + 4: idx + 6] + 
                                hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]), 32)
        
        idx += 8
        row['x_val'] = twos_complement((hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]), 16)

        idx += 4
        row['y_val'] = twos_complement((hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]), 16)

        idx += 4
        row['z_val'] = twos_complement((hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]), 16)

        final_data.append(row)

    return final_data
