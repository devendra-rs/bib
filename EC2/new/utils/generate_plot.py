import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def generate_plt(audio=np.array([]), abc=pd.DataFrame(), type_='', file_name='', sr=0):
    if type_=='audio':
        # Generate time data
        x_audio = np.linspace(
                0, # start
                len(audio) / sr,
                num = len(audio)
            )

        plt.plot(x_audio, audio)
    elif type_ == 'accl':
        # Generate time data
        x = np.linspace(
                0, # start
                (abc.second_epoch_int.max() - abc.second_epoch_int.min()),
                num = len(abc)
            ) 

        sample_rate = len(abc) / (abc.second_epoch_int.max() - abc.second_epoch_int.min())
        abc['adjusted_x'] = abc.x_val.values
        abc.loc[abc.adjusted_x == 0, "adjusted_x"] = abc['adjusted_x'].mean()
        a = abc["adjusted_x"]

        abc['adjusted_y'] = abc.y_val.values
        abc.loc[abc.adjusted_y == 0, "adjusted_y"] = abc['adjusted_y'].mean()
        b = abc["adjusted_y"]

        abc['adjusted_z'] = abc.z_val.values
        abc.loc[abc.adjusted_z == 0, "adjusted_z"] = abc['adjusted_z'].mean()
        c = abc["adjusted_z"]

        fig, ax = plt.subplots(3)
        ax[0].plot(x, a)
        ax[1].plot(x, b)
        ax[2].plot(x, c)
    else:
        return False
    plt.savefig(file_name)
    plt.clf()
    return True

