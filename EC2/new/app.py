import os
import sys
import time
import librosa
import numpy as np
import pandas as pd

import threading
import concurrent.futures
import matplotlib.pyplot as plt

from audio_processing import *
from utils.download_file import download_file
from utils.generate_plot import generate_plt
from utils.store_data import store_data
from utils.accelerometer_data_loading import binary_data_loading

from flask import Flask, request, jsonify
app = Flask(__name__)


@app.route('/', methods=['POST'])
def receive_data():

    # Flag to check if accelerometer file is available
    acc_avail = False

    # Flag to check if audio file is available
    audio_avail = False

    # Flag to check if the file received is accelerometer
    acc_initial = False

    file_to_download = request.get_json()
    if ".bin" in file_to_download:
        # File received is accelerometer
        acc_initial = True
        file_to_download = file_to_download.split("ACL")[0] + "SDR_1.mp3"

    print(file_to_download)

    try:
        # Download audio file if not already downloaded
        if file_to_download not in os.listdir():
            file_downloaded = download_file(file_to_download)
        if acc_initial == False:
            # Load audio file and turn audio available flag to true if the file received was not accelerometer
            audio, sr = librosa.load(f'./{file_to_download}', sr=44100)
            audio_avail = True
    except:
        pass

    try:
        # Download accelerometer file if not already downloaded
        acc_file = file_to_download.split('SDR')[0] + "ACL_1.bin"
        if acc_file not in os.listdir():
            print(f"Downloading: {acc_file}")
            file_downloaded = download_file(acc_file)
            print('file downloaded')

        # Load accelerometer data and turn accelerometer available flag true
        ab = binary_data_loading(acc_file)
        abc = pd.DataFrame(ab)
        acc_avail = True
    except Exception as e:
        print(str(e))



    print("file loaded")
    start_time = time.time()

    # Flag to check if there was no error in audio processing
    got_result = False

    content = f"No Files Found"
    # content = (f"No Files Found")

    try:
        if audio_avail:
            # Process audio
            with concurrent.futures.ThreadPoolExecutor(max_workers=None) as executor:
                future = executor.submit(process_audio, file_to_download, audio, sr)
                result = future.result()

            print(result)
            got_result = True

            if acc_avail:
                # Set content of mail if both acceleromter and audio file available and audio processed without error
                content = f"Length of accelerometer file: {round(abc.second_epoch_int.max() - abc.second_epoch_int.min())} seconds, length of sound file: {round(len(audio) / sr)} seconds, number of gulps: {result['Gulps']}"
            else:
                # Set content of mail if only audio file available
                content = (f"Length of sound file: {round(len(audio) / sr)} seconds, number of gulps: {result['Gulps']}")
        else:
            if acc_avail:
                # Set content of mail if only acceleromter file available
                content = (f"Length of accelerometer file: {round(abc.second_epoch_int.max() - abc.second_epoch_int.min())} seconds")
            else:
                # Set content of mail if no file available
                content = (f"No Files Found")
    except:
        if acc_avail:
            # Set content of mail if both acceleromter and audio file available but audio was not processed without error
            content = f"Length of accelerometer file: {round(abc.second_epoch_int.max() - abc.second_epoch_int.min())} seconds, length of sound file: {round(len(audio) / sr)} seconds"
        else:
            # Set content of mail if no file available
            content = f"No Files Found"


    elapsed_time = time.time() - start_time

    print(f"Elapsed time: {elapsed_time:.2f} seconds")

    # Set subject
    subject = "Accelerometer and sound file for " + file_to_download.split('SDR')[0]

    # genereate accelerometer plot
    if acc_avail:
        acc_image: bool = generate_plt(abc=abc, type_='accl', file_name=acc_file.split('.')[0]+'.png')

    # genereate audio plot
    if audio_avail:
        audio_image: bool = generate_plt(audio=audio, type_='audio', file_name=file_to_download.split('.')[0]+'.png', sr=sr)

    # attach images
    images_to_attach = []
    if acc_avail: images_to_attach.append(acc_file.split('.')[0]+'.png')
    if audio_avail: images_to_attach.append(file_to_download.split('.')[0]+'.png')

    # send mail
    mail_sent = send_mail(content_text=content, subject=subject, images_to_add=images_to_attach)

    if got_result:
        try:
            # store data in RDS
            id_ = store_data(file_to_download, result)
        except:
            id_ = -1

        # Return these results if computation happened
        return {"Volume": (result['Volume']), "Gulps": result['Gulps'], "Mail sent": mail_sent, "Accelerometer found": acc_avail, "Audio found": audio_avail, "changed_id": id_}, 201
    else:
        # Return these results if computation could not happen
        return {"Volume": 0, "Gulps": 0, "Mail sent": mail_sent, "Accelerometer found": acc_avail, "Audio found": audio_avail, "changed_id": -1}, 201


if __name__ == '__main__':
    app.run()

