import json
import urllib
import requests
from urllib import request
import json


def lambda_handler(event, context):
    
    bucket = event['Records'][0]['s3']['bucket']['name']
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')

    try:
        data = key

        # EC2 url
        url = "http://10.0.2.52/"
        headers = {'content-type': 'application/json'}

        # send post request
        response = requests.post(url=url, json=data, headers=headers)

        # parse response
        resp = response.json()

        return {
            'statusCode': 200,
            'body': json.dumps(
                {
                    "changed row": resp['changed_id'],
                }
            )
        }
    except Exception as e:
        return {
            'statusCode': 400,
            'body': json.dumps(
                {
                    "error": str(e),
                }
            )
        }
