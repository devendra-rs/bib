# This repository consists of following folders

## EC2:
- The running script i.e. app.py.
- Functions required for audio processing, such as audio_processing.py, frame_processing.py.
- Trained models. e.g. gulp_detection_model_2.sav.
- Other utility Functions.

## experiments:
- Consists of various scripts developed during progression.

## lambda:
- Script on AWS lambda service.

## various_models:
- Scripts consisting of training and evaluation of various models, such as CNN, LCNN, ResNets, GMM, etc.

# Code execution flow

## Overall flow
![Overall application flow](./BIB_Flow.png?raw=true "Application flow")

## EC2 flow
![Ec2 flow](./EC2_Flow.png?raw=true "EC2 code execution")


```
Psuedocode for code running in EC2:

1. Import necessary libraries and modules
2. Import required functions and constants

3. Define process_audio function to process audio files and calculate the number of gulps and volume
4. Define process_frame function to process audio frames and extract features
5. Define binary_data_loading function to read accelerometer data from binary files
6. Define download_file function to download files from AWS S3 bucket
7. Define generate_plt function to generate plots for audio and accelerometer data
8. Define send_mail function to send emails with text content and attached images
9. Define num_to_sec function to convert a number to seconds based on the sample rate
10. Define store_data function to store results in a database
11. Define twos_complement function to calculate the two's complement of a hexadecimal value

12. Initialize a Flask app

13. Define receive_data function for the POST request at the root endpoint:
    a. Initialize variables for accelerometer and audio availability and accelerometer initial status
    b. Get the file_to_download from the request JSON and extract the file name
    c. Check if the file is an accelerometer file and update the file_to_download with the corresponding audio file name
    d. Download and load the audio and accelerometer files, and update the availability variables accordingly
    e. Process the audio and get the result using ThreadPoolExecutor and process_audio function
    f. Update the content text based on the availability of audio and accelerometer data
    g. Calculate the elapsed time for processing
    h. Set the email subject using the file_to_download
    i. Generate plots for accelerometer and audio data if available
    j. Attach the generated images to the email and send it using send_mail function
    k. Store the result in the database and return the result and status as a JSON response

14. Run the Flask app
```

## Lambda

```
Psuedocode for app_function.py:

1. Import the following libraries: json, urllib, requests
2. Define a function called lambda_handler with the parameters event and context
   1. Get the bucket name from the event object
   2. Get the key from the event object and decode it using utf-8 encoding
   3. Use try-except block
      1. Inside try block
         1. Set data variable to key
         2. Set url variable to the desired URL
         3. Set headers variable to the content-type 'application/json'
         4. Send a POST request to the URL with the data and headers
         5. Get the JSON response from the request
         6. Return the status code 200 and the changed row from the response as the body
      2. Inside except block
         1. Catch any exception as e
         2. Return the status code 400 and the error message as the body
```
