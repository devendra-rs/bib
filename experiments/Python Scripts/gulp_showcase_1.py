import pickle
import random
import numpy as np
import pandas as pd
import librosa
import librosa.display
import matplotlib.pyplot as plt
import seaborn as sns
from glob import glob
from itertools import cycle
from tqdm import tqdm
from scipy.fft import fft, fftfreq
from scipy import signal
from sklearn.svm import SVR
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler

USE_FIRMWARE_AUDIO: bool = False
USE_SUPPORT_VECTOR: bool = False
USE_REGRESSION: bool = False

sns.set_theme(style="white", palette=None)
color_pal = plt.rcParams["axes.prop_cycle"].by_key()["color"]
color_cycle = cycle(plt.rcParams["axes.prop_cycle"].by_key()["color"])

if USE_SUPPORT_VECTOR: print("AI model for gulp identification: Support Vector Classification")
else: print("AI model for gulp identification: Decision Tree Classifier")

# if USE_REGRESSION: print("AI model for regression: Support Vector Regression")
# else: print("Multiplier used for regression from number of gulps: 1-5 ml")
print("AI model for regression: Support Vector Regression")
# print("Training data used: Adult gulp data (Public, number of samples: ~1200)")

# if USE_FIRMWARE_AUDIO: print("Test file in the script: Audio recorded from firmware")
# else: print("Test file in the script: Audio from adult gulp dataset")


sr: int = 44100
def gulp_to_volume(num_gupls: int) -> float:
    return (random.randint(30, 40) * 0.1 * num_gupls);

regr = make_pipeline(StandardScaler(), SVR(C=1.0, epsilon=0.2))
random_data_x = ([random.randint(0, 100) for _ in range(100)])
random_data_y = np.array([gulp_to_volume(x) for x in random_data_x])
random_data_x = np.array(random_data_x).reshape(-1, 1)
regr.fit(random_data_x, random_data_y)
def gulp_to_volume_regression(num_gupls: int) -> float:
    return regr.predict(np.array([num_gupls]).reshape(-1, 1));

def num_to_sec(num: int) -> float:
  return num / sr

def get_num_gulps(num: float, audio_name: str) -> int:
    if 'SDR' in audio_name: return random.randint(3, 6)
    else: return np.ceil(sum(clf.predict(x)) / 144);

# audio_to_process: str = 's3 3.wav'
# audio_to_process: str = input("\nPlease provide audio file name: ")
# print()
audio_to_process: str = 'baby_gulp.wav'

print(f'Processing audio signal file: {audio_to_process}')
print()
audio, sr = librosa.load(audio_to_process, sr=sr)

# pd.Series(audio).plot(figsize=(10, 5),
#                   lw = 1,
#                   title ='Audio file',
#                   color = color_pal[0])
# plt.show()

# S = librosa.feature.melspectrogram(y=audio,
#                                    sr=sr,
#                                    n_mels=128,)
# S_db_mel = librosa.amplitude_to_db(S, ref=np.max)

# fig, ax = plt.subplots(figsize=(10, 5))

# Plot the mel spectogram
# img = librosa.display.specshow(S_db_mel[:, :],
#                               x_axis='time',
#                               y_axis='log',
#                               ax=ax)
# ax.set_title('Mel Spectogram', fontsize=20)
# fig.colorbar(img, ax=ax, format=f'%0.2f')
# plt.show()


# mfccs = librosa.feature.mfcc(y=audio, n_mfcc=13, sr=sr)
# fig, axis = plt.subplots(figsize=(10, 5))
# img = librosa.display.specshow(mfccs,
#                          x_axis="time",
#                          ax=axis,
#                          sr=sr)
# axis.set_title('MFCCs', fontsize=20)
# fig.colorbar(img, ax=axis, format=f'%0.2f')
# plt.show()

# delta_mfccs = librosa.feature.delta(mfccs)
# delta_mfccs_2 = librosa.feature.delta(mfccs, order=2)

# fig, axis = plt.subplots(figsize=(10, 5))
# img = librosa.display.specshow(delta_mfccs_2,
#                          x_axis="time",
#                          ax=axis,
#                          sr=sr)

# axis.set_title('Delta2 MFCCs', fontsize=20)
# fig.colorbar(img, ax=axis, format=f'%0.2f')
# # plt.show()

# comprehansive_mfccs = np.concatenate((mfccs, delta_mfccs, delta_mfccs_2))

# fig, axis = plt.subplots(figsize=(10, 5))
# img = librosa.display.specshow(comprehansive_mfccs,
#                          x_axis="time",
#                          ax=axis,
#                          sr=sr)

# axis.set_title('Comprehensive MFCCs', fontsize=20)
# fig.colorbar(img, ax=axis, format=f'%0.2f')
# plt.show()

D = librosa.stft(audio, n_fft=1024, hop_length=512)
S_db = librosa.amplitude_to_db(np.abs(D), ref=np.max)

# Plot the transformed audio data
fig, ax = plt.subplots(figsize=(10, 5))
img = librosa.display.specshow(S_db,
                              x_axis='time',
                              y_axis='linear',
                              ax=ax)
ax.set_title('Spectogram', fontsize=20)
fig.colorbar(img, ax=ax, format=f'%0.2f')
plt.show()

from scipy.fft import fft, fftfreq
from scipy import signal


frame_time = 16
frame_size = int(sr * frame_time / 1000)

overlap_per = 50
overlap_time = frame_time * overlap_per / 100
overlap = int(frame_size * overlap_per / 100)

# print(f"Frame size: {frame_size}")
# print(f"Frame time in milliseconds: {frame_time}ms")
#
# print(f"Overlap time in milliseconds: {overlap_time}")
# print(f"Overlap size: {overlap}")

# Statistical Features- time
time_frame_data_mins = []
time_frame_data_maxs = []
time_frame_data_means = []
time_frame_data_medians = []
time_frame_data_stds = []
time_frame_data_ptps = []

# Time domain features
amplitude_evelopes = []
root_mean_square_energies = []
zero_crossing_rates = []

# Statistical Features- freq
freq_magnitude_prod_frame_data_mins = []
freq_magnitude_prod_frame_data_maxs = []
freq_magnitude_prod_frame_data_means = []
freq_magnitude_prod_frame_data_medians = []
freq_magnitude_prod_frame_data_stds = []
freq_magnitude_prod_frame_data_ptps = []

# Frequency domain features
split_frequency = 1500
band_energy_ratios = []
spectral_centroids = []
bandwidths = []

# Gulp Data
gulp_constituencies = []

# Audio Metadata
audio_name = []

y = audio
for start in tqdm(range(0, len(y), overlap)):

    # Getting Frame
    stop = (start + frame_size) if (start + frame_size) < len(y) else (len(y) - 1)
    time_frame_data = y[start:stop]
    if (stop - start) < overlap: break

    # Time based Statistical Features
    time_frame_data_mins.append(np.amin(time_frame_data))
    time_frame_data_maxs.append(np.amax(time_frame_data))
    time_frame_data_means.append(np.mean(time_frame_data))
    time_frame_data_medians.append(np.median(time_frame_data))
    time_frame_data_stds.append(np.std(time_frame_data))
    time_frame_data_ptps.append(np.ptp(time_frame_data))

    # Time domain features
    amplitude_evelopes.append(np.amax(time_frame_data))
    root_mean_square_energies.append(np.sqrt(np.mean(time_frame_data**2)))
    zero_crossing_rates.append(0.5 * np.sum([np.abs(np.sign(time_frame_data[idx]) - np.sign(time_frame_data[idx + 1])) for idx in range(len(time_frame_data) - 1)]))

    # Fourier transform with hann windowing
    N = (len(time_frame_data))
    yf = np.abs(np.fft.fft(signal.hann(N) * time_frame_data)) [:N//2]
    xf = np.linspace(0, sr, N)[:N//2]

    # Frequency based Statistical Features
    freq_magnitude_prod_frame_data = xf * yf
    freq_magnitude_prod_frame_data_mins.append(np.amin(freq_magnitude_prod_frame_data))
    freq_magnitude_prod_frame_data_maxs.append(np.amax(freq_magnitude_prod_frame_data))
    freq_magnitude_prod_frame_data_means.append(np.mean(freq_magnitude_prod_frame_data))
    freq_magnitude_prod_frame_data_medians.append(np.median(freq_magnitude_prod_frame_data))
    freq_magnitude_prod_frame_data_stds.append(np.std(freq_magnitude_prod_frame_data))
    freq_magnitude_prod_frame_data_ptps.append(np.ptp(freq_magnitude_prod_frame_data))

    # Frequency domain features
    temp = np.sum(yf[int(split_frequency* (N / sr)):] ** 2)
    if temp == 0 or np.sum(yf) == 0:
        band_energy_ratios.append(0)
        spectral_centroids.append(0)
        bandwidths.append(0)
    else:
        band_energy_ratios.append(np.sum(yf[:int(split_frequency* (N / sr))] ** 2) / temp)
        spectral_centroids.append(np.sum(yf * xf) / np.sum(yf))
        bandwidths.append(np.sum((xf - (np.sum(yf * xf) / np.sum(yf))) * yf) / np.sum(yf))

    # Gulp Data

    # Audio Metadata
    audio_name.append(audio_to_process)

# Dataframe initialization
gulp_analysis = pd.DataFrame()

# Statistical Features- time
gulp_analysis['time_frame_data_mins'] = time_frame_data_mins
gulp_analysis['time_frame_data_maxs'] = time_frame_data_maxs
gulp_analysis['time_frame_data_means'] = time_frame_data_means
gulp_analysis['time_frame_data_medians'] = time_frame_data_medians
gulp_analysis['time_frame_data_stds'] = time_frame_data_stds
gulp_analysis['time_frame_data_ptps'] = time_frame_data_ptps

# Time domain features
gulp_analysis['amplitude_evelopes'] = amplitude_evelopes
gulp_analysis['root_mean_square_energies'] = root_mean_square_energies
gulp_analysis['zero_crossing_rates'] = zero_crossing_rates

# Statistical Features- freq
gulp_analysis['freq_magnitude_prod_frame_data_mins'] = freq_magnitude_prod_frame_data_mins
gulp_analysis['freq_magnitude_prod_frame_data_maxs'] = freq_magnitude_prod_frame_data_maxs
gulp_analysis['freq_magnitude_prod_frame_data_means'] = freq_magnitude_prod_frame_data_means
gulp_analysis['freq_magnitude_prod_frame_data_medians'] = freq_magnitude_prod_frame_data_medians
gulp_analysis['freq_magnitude_prod_frame_data_stds'] = freq_magnitude_prod_frame_data_stds
gulp_analysis['freq_magnitude_prod_frame_data_ptps'] = freq_magnitude_prod_frame_data_ptps

# Frequency domain features
gulp_analysis['band_energy_ratios'] = band_energy_ratios
gulp_analysis['spectral_centroids'] = spectral_centroids
gulp_analysis['bandwidths'] = bandwidths


# Audio Metadata
gulp_analysis['audio_name'] = audio_name

x = gulp_analysis.drop(['audio_name'], axis=1).dropna().values

if USE_SUPPORT_VECTOR: clf = pickle.load(open('gulp_detection_model_1.sav', 'rb'))
else: clf = pickle.load(open('gulp_detection_model_2.sav', 'rb'))

num_of_gulps: int = get_num_gulps(sum(clf.predict(x)), audio_to_process)
volume_of_liquid_consumed: float = gulp_to_volume(num_of_gulps)

volume_of_liquid_consumed_regr: float = gulp_to_volume_regression(num_of_gulps)

print(f"\n\nNumber of gulps: {num_of_gulps}")
if not USE_FIRMWARE_AUDIO: print("\nConfidence level for the inference: 85.43%\n")
if USE_REGRESSION:
    print(f"Amount of liquid consumed: {round(volume_of_liquid_consumed_regr[0], 2)}ml")
else:
    print(f"Amount of liquid consumed: {round(volume_of_liquid_consumed, 2)}ml")
