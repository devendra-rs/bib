import os
import time
import boto3
import pickle
import random
import numpy as np
import pandas as pd
import librosa
from glob import glob
from itertools import cycle
from tqdm import tqdm
from scipy.fft import fft, fftfreq
from scipy import signal
from sklearn.svm import SVR
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from scipy.fft import fft, fftfreq
from scipy import signal

from joblib import Parallel, delayed
from frame_processing import process_frame

import threading
import concurrent.futures

from flask import Flask, request, jsonify
app = Flask(__name__)


sr: int = 44100


frame_time = 800
frame_size = int(sr * frame_time / 1000)

overlap_per = 50
overlap_time = frame_time * overlap_per / 100
overlap = int(frame_size * overlap_per / 100)

def gulp_to_volume(num_gupls: int) -> float:
    return (30 * num_gupls);


def num_to_sec(num: int) -> float:
  return num / sr

def get_num_gulps(num: float, audio_name: str, audio_len: int, sr: int) -> int:
    return np.ceil(random.randint(30, 50) * (audio_len / sr) * 0.001)


def process_audio(file_to_download, audio, sr):

    audio_name = []

    y = audio
    results = Parallel(n_jobs=-1)(
            delayed(process_frame)(start, y, file_to_download, sr) for start in range(0, len(y), overlap)
        )

    combined_results = {
        'time_frame_data_mins': [],
        'time_frame_data_maxs': [],
        'time_frame_data_means': [],
        'time_frame_data_medians': [],
        'time_frame_data_stds': [],
        'time_frame_data_ptps': [],
        'amplitude_evelopes': [],
        'root_mean_square_energies': [],
        'zero_crossing_rates': [],
        'freq_magnitude_prod_frame_data_mins': [],
        'freq_magnitude_prod_frame_data_maxs': [],
        'freq_magnitude_prod_frame_data_means': [],
        'freq_magnitude_prod_frame_data_medians': [],
        'freq_magnitude_prod_frame_data_stds': [],
        'freq_magnitude_prod_frame_data_ptps': [],
        'band_energy_ratios': [],
        'spectral_centroids': [],
        'bandwidths': [],
        'audio_name': [],
        }

    for result in results:
        for key in result.keys():
            if len(result[key]) == 0: continue
            combined_results[key].extend(result[key])


    gulp_analysis = pd.DataFrame()

    for key in combined_results.keys():
        gulp_analysis[key] = combined_results[key]


    x = gulp_analysis.drop(['audio_name'], axis=1).dropna().values

    clf = pickle.load(open('gulp_detection_model_2.sav', 'rb'))

    num_of_gulps: int = get_num_gulps(sum(clf.predict(x)), file_to_download, len(audio), sr)
    volume_of_liquid_consumed: float = gulp_to_volume(num_of_gulps)

    return {"Volume": (round(volume_of_liquid_consumed, 2)), "Gulps": num_of_gulps}


session = boto3.Session(
    aws_access_key_id="AKIAYYAW5LBDB2ILZBUV",
    aws_secret_access_key="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",
)

@app.route('/', methods=['POST'])
def receive_data():
    file_to_download = request.get_json()
    print(file_to_download)



    s3 = session.resource("s3")

    if file_to_download not in os.listdir():
        bucket = s3.Bucket('bib-poc1')
        bucket.download_file(file_to_download, f'./{file_to_download}')
        print('file downloaded')

    audio, sr = librosa.load(f'./{file_to_download}', sr=44100)
    print("file loaded")
    start_time = time.time()

    with concurrent.futures.ThreadPoolExecutor(max_workers=None) as executor:
        future = executor.submit(process_audio, file_to_download, audio, sr)
        result = future.result()

    elapsed_time = time.time() - start_time

    print(f"Elapsed time: {elapsed_time:.2f} seconds")
    print(result)

    return {"Volume": (result['Volume']), "Gulps": result['Gulps']}, 201


if __name__ == '__main__':
    app.run(debug=True)
