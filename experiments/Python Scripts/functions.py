from scipy.fft import fft, fftfreq
from scipy import signal
from sklearn.ensemble import RandomForestClassifier
from tqdm import tqdm
import numpy as np
import pandas as pd
import librosa
from scipy.fft import fft, fftfreq
from scipy import signal
import scipy

def static_feat_extract(audio_signal_test, sr):
    frame_time = 16
    frame_size = int(sr * frame_time / 1000)

    overlap_per = 50
    overlap_time = frame_time * overlap_per / 100
    overlap = int(frame_size * overlap_per / 100)

    print(f"Frame size: {frame_size}")
    print(f"Frame time in milliseconds: {frame_time}ms")

    print(f"Overlap time in milliseconds: {overlap_time}")
    print(f"Overlap size: {overlap}")

    # Statistical Features- time
    time_frame_data_mins = []
    time_frame_data_maxs = []
    time_frame_data_means = []
    time_frame_data_medians = []
    time_frame_data_stds = []
    time_frame_data_ptps = []

    # Time domain features
    amplitude_evelopes = []
    root_mean_square_energies = []
    zero_crossing_rates = []

    # Statistical Features- freq
    freq_magnitude_prod_frame_data_mins = []
    freq_magnitude_prod_frame_data_maxs = []
    freq_magnitude_prod_frame_data_means = []
    freq_magnitude_prod_frame_data_medians = []
    freq_magnitude_prod_frame_data_stds = []
    freq_magnitude_prod_frame_data_ptps = []

    # Frequency domain features
    split_frequency = 1500
    band_energy_ratios = []
    spectral_centroids = []
    bandwidths = []

    # Gulp Data
    gulp_constituencies = []

    # Audio Metadata
    audio_name = []

    for audio_signal in audio_signal_test.keys():
      print(f'Processing audio signal file: {audio_signal}')
      y = audio_signal_test[audio_signal]
      for start in tqdm(range(0, len(y), overlap)):

        # Getting Frame
        stop = (start + frame_size) if (start + frame_size) < len(y) else (len(y) - 1)
        time_frame_data = y[start:stop]
        if (stop - start) < overlap: break

        # Time based Statistical Features
        time_frame_data_mins.append(np.amin(time_frame_data))
        time_frame_data_maxs.append(np.amax(time_frame_data))
        time_frame_data_means.append(np.mean(time_frame_data))
        time_frame_data_medians.append(np.median(time_frame_data))
        time_frame_data_stds.append(np.std(time_frame_data))
        time_frame_data_ptps.append(np.ptp(time_frame_data))

        # Time domain features
        amplitude_evelopes.append(np.amax(time_frame_data))
        root_mean_square_energies.append(np.sqrt(np.mean(time_frame_data**2)))
        zero_crossing_rates.append(0.5 * np.sum([np.abs(np.sign(time_frame_data[idx]) - np.sign(time_frame_data[idx + 1])) for idx in range(len(time_frame_data) - 1)]))

        # Fourier transform with hann windowing
        N = (len(time_frame_data))
        yf = np.abs(np.fft.fft(signal.hann(N) * time_frame_data)) [:N//2]
        xf = np.linspace(0, sr, N)[:N//2]

        # Frequency based Statistical Features
        freq_magnitude_prod_frame_data = xf * yf
        freq_magnitude_prod_frame_data_mins.append(np.amin(freq_magnitude_prod_frame_data))
        freq_magnitude_prod_frame_data_maxs.append(np.amax(freq_magnitude_prod_frame_data))
        freq_magnitude_prod_frame_data_means.append(np.mean(freq_magnitude_prod_frame_data))
        freq_magnitude_prod_frame_data_medians.append(np.median(freq_magnitude_prod_frame_data))
        freq_magnitude_prod_frame_data_stds.append(np.std(freq_magnitude_prod_frame_data))
        freq_magnitude_prod_frame_data_ptps.append(np.ptp(freq_magnitude_prod_frame_data))

        # Frequency domain features
        band_energy_ratios.append(np.sum(yf[:int(split_frequency* (N / sr))] ** 2) / np.sum(yf[int(split_frequency* (N / sr)):] ** 2))
        spectral_centroids.append(np.sum(yf * xf) / np.sum(yf))
        bandwidths.append(np.sum((xf - (np.sum(yf * xf) / np.sum(yf))) * yf) / np.sum(yf))

        # Gulp Data
        # gulp_constituencies.append(int(len(gulp_data.loc[(gulp_data.audio == audio_signal) & (gulp_data.total_start_sec <= num_to_sec(start)) & (gulp_data.total_stop_sec >= num_to_sec(stop))]) > 0))

        # Audio Metadata
        audio_name.append(audio_signal)

    # Dataframe initialization
    gulp_analysis = pd.DataFrame()

    # Statistical Features- time
    gulp_analysis['time_frame_data_mins'] = time_frame_data_mins
    gulp_analysis['time_frame_data_maxs'] = time_frame_data_maxs
    gulp_analysis['time_frame_data_means'] = time_frame_data_means
    gulp_analysis['time_frame_data_medians'] = time_frame_data_medians
    gulp_analysis['time_frame_data_stds'] = time_frame_data_stds
    gulp_analysis['time_frame_data_ptps'] = time_frame_data_ptps

    # Time domain features
    gulp_analysis['amplitude_evelopes'] = amplitude_evelopes
    gulp_analysis['root_mean_square_energies'] = root_mean_square_energies
    gulp_analysis['zero_crossing_rates'] = zero_crossing_rates

    # Statistical Features- freq
    gulp_analysis['freq_magnitude_prod_frame_data_mins'] = freq_magnitude_prod_frame_data_mins
    gulp_analysis['freq_magnitude_prod_frame_data_maxs'] = freq_magnitude_prod_frame_data_maxs
    gulp_analysis['freq_magnitude_prod_frame_data_means'] = freq_magnitude_prod_frame_data_means
    gulp_analysis['freq_magnitude_prod_frame_data_medians'] = freq_magnitude_prod_frame_data_medians
    gulp_analysis['freq_magnitude_prod_frame_data_stds'] = freq_magnitude_prod_frame_data_stds
    gulp_analysis['freq_magnitude_prod_frame_data_ptps'] = freq_magnitude_prod_frame_data_ptps

    # Frequency domain features
    gulp_analysis['band_energy_ratios'] = band_energy_ratios
    gulp_analysis['spectral_centroids'] = spectral_centroids
    gulp_analysis['bandwidths'] = bandwidths

    # Gulp Data
    # gulp_analysis['gulp_constituencies'] = gulp_constituencies

    # Audio Metadata
    gulp_analysis['audio_name'] = audio_name

    return gulp_analysis


def lin(sr, n_fft, n_filter=128, fmin=0.0, fmax=None, dtype=np.float32):

    if fmax is None:
        fmax = float(sr) / 2
    # Initialize the weights
    n_filter = int(n_filter)
    weights = np.zeros((n_filter, int(1 + n_fft // 2)), dtype=dtype)

    # Center freqs of each FFT bin
    fftfreqs = librosa.fft_frequencies(sr=sr, n_fft=n_fft)

    # 'Center freqs' of liner bands - uniformly spaced between limits
    linear_f = np.linspace(fmin, fmax, n_filter + 2)

    fdiff = np.diff(linear_f)
    ramps = np.subtract.outer(linear_f, fftfreqs)

    for i in range(n_filter):
        # lower and upper slopes for all bins
        lower = -ramps[i] / fdiff[i]
        upper = ramps[i + 2] / fdiff[i + 1]

        # .. then intersect them with each other and zero
        weights[i] = np.maximum(0, np.minimum(lower, upper))

    return weights


def linear_spec(y=None,
                sr=22050,
                n_fft=2048,
                hop_length=512,
                win_length=None,
                window='hann',
                center=True,
                pad_mode='reflect',
                power=2.0,
                **kwargs):
    S = np.abs(
        librosa.core.stft(y=y,
                          n_fft=n_fft,
                          hop_length=hop_length,
                          win_length=win_length,
                          window=window,
                          center=center,
                          pad_mode=pad_mode))**power
    filter = lin(sr=sr, n_fft=n_fft, **kwargs)
    return np.dot(filter, S)


def lfcc(y=None,
         sr=22050,
         S=None,
         n_lfcc=20,
         dct_type=2,
         norm='ortho',
         **kwargs):
    if S is None:
        S = librosa.power_to_db(linear_spec(y=y, sr=sr, **kwargs))
    M = scipy.fftpack.dct(S, axis=0, type=dct_type, norm=norm)[:n_lfcc]
    return M


def compre_feats(audio_signals, sr):
    frame_time = 16
    frame_size = int(sr * frame_time / 1000)

    overlap_per = 50
    overlap_time = frame_time * overlap_per / 100
    overlap = int(frame_size * overlap_per / 100)
    # overlap = 0.008

    comprehensive_centroid = {}
    comprehensive_mfccs = {}
    comprehensive_lfccs = {}
    comprehensive_zcr = {}
    comprehensive_rf = {}

    comprehensive_features = {}
    for audio_signal in audio_signals.keys():
      # MFCCS
      mfccs = librosa.feature.mfcc(audio_signals[audio_signal], n_mfcc=13, sr=sr, n_fft=frame_size, hop_length=overlap)
      delta_mfccs = librosa.feature.delta(mfccs)
      delta_mfccs_2 = librosa.feature.delta(mfccs, order=2)
      comprehensive_mfccs[audio_signal] = np.concatenate((mfccs, delta_mfccs, delta_mfccs_2))

      # LFCCS
      lfccs = lfcc(y=audio_signals[audio_signal], sr=sr, n_fft=frame_size, hop_length=overlap)
      delta_lfccs = librosa.feature.delta(lfccs)
      delta_lfccs_2 = librosa.feature.delta(lfccs, order=2)
      comprehensive_lfccs[audio_signal] = np.concatenate((lfccs, delta_lfccs, delta_lfccs_2))

      # Zero Crossing Rate
      zcr = librosa.feature.zero_crossing_rate(audio_signals[audio_signal], frame_length=frame_size, hop_length=overlap)
      delta_zcr = librosa.feature.delta(zcr)
      delta_zcr_2 = librosa.feature.delta(zcr, order=2)
      comprehensive_zcr[audio_signal] = np.concatenate((zcr, delta_zcr, delta_zcr_2))

      # Roll off
      rf = librosa.feature.spectral_rolloff(audio_signals[audio_signal], sr=sr, n_fft=frame_size, hop_length=overlap)
      delta_rf = librosa.feature.delta(rf)
      delta_rf_2 = librosa.feature.delta(rf, order=2)
      comprehensive_rf[audio_signal] = np.concatenate((rf, delta_rf, delta_rf_2))

      # Centroid
      centroid = librosa.feature.spectral_centroid(audio_signals[audio_signal], sr=sr, n_fft=frame_size, hop_length=overlap)
      delta_centroid = librosa.feature.delta(centroid)
      delta_centroid_2 = librosa.feature.delta(centroid, order=2)
      comprehensive_centroid[audio_signal] = np.concatenate((centroid, delta_centroid, delta_centroid_2))

      # All Features
      comprehensive_features[audio_signal] = np.concatenate((mfccs, delta_mfccs, delta_mfccs_2,
                                               lfccs, delta_lfccs, delta_lfccs_2,
                                               zcr, delta_zcr, delta_zcr_2,
                                               rf, delta_rf, delta_rf_2,
                                               centroid, delta_centroid, delta_centroid_2))
    
    return comprehensive_features