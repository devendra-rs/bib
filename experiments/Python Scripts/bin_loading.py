import time
import sys


def binary_data_loading(file=''):
    if file == '': return []
    hex_data = open(file, 'rb').read().hex()
    final_data = []
    for base_idx in range(0, len(hex_data), 28):
        row = {
                'second_epoch': 0,
                'time': '',
                'millisecond_epoch': 0,
                'x_val': 0,
                'y_val': 0,
                'z_val': 0
                }

        idx = base_idx
        row['second_epoch'] = int((hex_data[idx + 6: idx + 8] + hex_data[idx + 4: idx + 6] + hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]), 16)
        row['time'] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(row['second_epoch']))


        idx += 8
        row['millisecond_epoch'] = int((hex_data[idx + 6: idx + 8] + hex_data[idx + 4: idx + 6] + hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]), 16)

        idx += 8    
        row['x_val'] = int((hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]), 16)

        idx += 4
        row['y_val'] = int((hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]), 16)

        idx += 4
        row['z_val'] = int((hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]), 16)

        final_data.append(row)

    return final_data

if len(sys.argv) > 1:
    print(sys.argv)
    file = sys.argv[1]
    print(binary_data_loading(file))
