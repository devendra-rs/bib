import os
import copy
import torch
import random
import librosa
import argparse
import torchaudio
import numpy as np
import pandas as pd
from glob import glob
from scipy import signal
from torchaudio import transforms
import torchaudio.functional as F
from collections import namedtuple
from scipy.fft import fft, fftfreq
from audiomentations import (Compose, AddGaussianNoise, TimeStretch, PitchShift, Shift,
SpecCompose, SpecChannelShuffle, SpecFrequencyMask)

# Load directory data
audio_files = glob('./*.wav')
label_files = glob('./*.csv')

# Load Audios
sr = 44100
audio_signals = {}
for audio_file in audio_files:
  audio_signals[(audio_file.split('/')[-1].split('.')[0].split(' ')[0])
                 ], _ = librosa.load(audio_file)

# Load Labels
gulp_data = pd.DataFrame()
for csv in (label_files):
  gulp_datum = pd.read_csv(csv)
  gulp_datum['audio'] = csv[2:4]
  gulp_datum[['start_min', 'start_sec', 'start_millisec']] = gulp_datum['start'].str.split('.', 2,expand=True)
  gulp_datum['total_start_sec'] = gulp_datum.start_min.astype(int) * 60 + gulp_datum.start_sec.astype(int) + gulp_datum.start_millisec.astype(int) / 1000
  gulp_datum[['stop_min', 'stop_sec', 'stop_millisec']] = gulp_datum['stop'].str.split('.', 2,expand=True)
  gulp_datum['total_stop_sec'] = gulp_datum.stop_min.astype(int) * 60 + gulp_datum.stop_sec.astype(int) + gulp_datum.stop_millisec.astype(int) / 1000
  gulp_data = gulp_data.append(gulp_datum)

def time_to_num(seconds = 0):
  return int(seconds * sr)

gulp_data['start_num'] = gulp_data.total_start_sec.apply(time_to_num).values
gulp_data['stop_num'] = gulp_data.total_stop_sec.apply(time_to_num).values

# Load Gulps
gulp_frames = []
for idx, row in gulp_data.iterrows():
  gulp_frames.append(audio_signals[row.audio][row.start_num:row.stop_num])


# Augmentation

# White noise addition
def add_white_noise(audio_signal, noise_factor):
    noise = np.random.normal(0, audio_signal.std(), audio_signal.size)
    augmented_signal = audio_signal + noise * noise_factor
    return augmented_signal

# Time Streching
def time_stretch(audio_signal, stretch_rate):
  if len(audio_signal) == 0: return []
  return librosa.effects.time_stretch(audio_signal, stretch_rate)

# Pitch Scaling
def pitch_scale(audio_signal, sr, num_semitones):
  return librosa.effects.pitch_shift(audio_signal, sr, num_semitones)

# Polarity Inversion
def invert_polarity(audio_signal):
  return audio_signal * -1

# Random Gain
def random_gain(audio_signal, min_gain_factor, max_gain_factor):
  gain_factor = random.uniform(min_gain_factor, max_gain_factor)
  return audio_signal * gain_factor


# Audiomentation Waveform
augment_waveform = Compose([
    AddGaussianNoise(min_amplitude=0.001, max_amplitude=0.015, p=0.5),
    TimeStretch(min_rate=0.8, max_rate=1.25, p=0.5),
    PitchShift(min_semitones=-4, max_semitones=4, p=0.5),
    Shift(min_fraction=-0.5, max_fraction=0.5, p=0.5),
])

# Audiomentation Spectogram
augment_spectogram = SpecCompose(
    [
        SpecChannelShuffle(p=0.5),
        SpecFrequencyMask(p=0.5),
    ]
)

# Torch audio signals
torch_audio_signals={}
for audio_file in audio_files:
  torch_audio_signals[audio_file[2:4]], sr=torchaudio.load(audio_file)

# Room reverb
effects=[
    ["lowpass", "-1", "300"],
    ["rate", f"{sr}"],
    ["reverb", "-w"],
]

# SpecAugment

# Frequency Masking
spectrogram=transforms.Spectrogram()
masking=transforms.FrequencyMasking(freq_mask_param=80)

# Time Masking
spectrogram=torchaudio.transforms.Spectrogram()
masking=torchaudio.transforms.TimeMasking(time_mask_param=80)

# Time Streching
spectrogram=torchaudio.transforms.Spectrogram()
stretch=torchaudio.transforms.TimeStretch()

# Apply augmentation

augmented_data={}

augmented_data['white_nose_low'] = []
augmented_data['white_nose_mid'] = []
augmented_data['white_nose_high'] = []
augmented_data['time_stretch_low'] = []
augmented_data['time_stretch_mid'] = []
augmented_data['time_stretch_high'] = []
augmented_data['pitch_scaling_low'] = []
augmented_data['pitch_scaling_mid'] = []
augmented_data['pitch_scaling_high'] = []
augmented_data['polarity_inverted'] = []
augmented_data['random_gain_low'] = []
augmented_data['random_gain_mid'] = []
augmented_data['random_gain_high'] = []
augmented_data['audiomentations_waveform'] = []
augmented_data['audiomentations_spectogram'] = []

for gulp_frame in gulp_frames:
    data=gulp_frame
    if len(data) < 100: continue

    # White noise
    augmented_data['white_nose_low'].append(add_white_noise(data, 0.1))
    augmented_data['white_nose_mid'].append(add_white_noise(data, 0.3))
    augmented_data['white_nose_high'].append(add_white_noise(data, 0.5))

    # Time Streching
    augmented_data['time_stretch_low'].append(time_stretch(data, 0.1))
    augmented_data['time_stretch_mid'].append(time_stretch(data, 0.3))
    augmented_data['time_stretch_high'].append(time_stretch(data, 0.5))

    # Pitch Scaling
    augmented_data['pitch_scaling_low'].append(pitch_scale(data, sr, 1))
    augmented_data['pitch_scaling_mid'].append(pitch_scale(data, sr, 2))
    augmented_data['pitch_scaling_high'].append(pitch_scale(data, sr, 2))

    # Polarity inversion
    augmented_data['polarity_inverted'].append(invert_polarity(data))

    # Random Gain
    augmented_data['random_gain_low'].append(random_gain(data, 0, 2))
    augmented_data['random_gain_mid'].append(random_gain(data, 2, 4))
    augmented_data['random_gain_high'].append(random_gain(data, 4, 6))

    # Audiomentation

    ## WaveForm
    augmented_data['audiomentations_waveform'].append(augment_waveform(samples=data, sample_rate=sr))

    ## Spectograph
    # spectrograph = np.abs(np.fft.fft(signal.hann(len(data)) * data))[:(len(data))//2]
    # augmented_data['audiomentations_spectogram'].append(augment_spectogram(spectrograph))


# Get non gulp data
non_gulp_data = []
start_space = 0
spacing = 25000
stop_space = start_space + spacing
number_of_gulp_samples_ex_per_signal = 100
for audio_signal_key in audio_signals.keys():
    for _ in range(number_of_gulp_samples_ex_per_signal):
        while len(gulp_data.loc[(gulp_data.audio=='s1') & (gulp_data.start_num
            <= start_space) & (gulp_data.stop_num >= stop_space)]) > 0:
            spacing = random.randint(10000, 25000);
            start_space = random.randint(0, len(audio_signals[audio_signal_key]) - spacing);
            stop_space = (start_space + spacing);
        non_gulp_data += (list(audio_signals[audio_signal_key][start_space:stop_space]))


# Get augmented gulp signals
segmented_augmented_gulp_signals = []
for augmented_data_key in augmented_data.keys():
    for val in augmented_data[augmented_data_key]:
        segmented_augmented_gulp_signals.append(val)

random.shuffle(segmented_augmented_gulp_signals)

temp_augmented_data = non_gulp_data
length_of_non_gulps = len(non_gulp_data)
for segmented_augmented_gulp_signal in segmented_augmented_gulp_signals:
    to_insert_idx = random.randint(0, length_of_non_gulps)
    temp_augmented_data.insert(to_insert_idx, list(segmented_augmented_gulp_signal))

buffer = 0
gulp_starts = []
gulp_stops = []
augmented_data = []
for idx in range(len(temp_augmented_data)):
    if type(temp_augmented_data[idx]) == float: augmented_data.append(temp_augmented_data[idx])
    elif type(temp_augmented_data[idx]) == list:
        gulp_starts.append(idx + buffer)
        buffer += len(temp_augmented_data[idx])
        gulp_stops.append(idx + buffer)
        augmented_data += temp_augmented_data[idx]
    else: augmented_data.append(temp_augmented_data[idx])

augmented_data_labels = pd.DataFrame()
augmented_data_labels["gulp_starts"] = gulp_starts
augmented_data_labels["gulp_stops"] = gulp_stops


# Model pipelines- Data transform, Model Loading, Model training, Model saving, Test result return

## Statistical Modelling
def run_through_statistical_model(data):
     # Copy all the code for statistical modelling also see how the dataframe is

## GMM HMM - Kapil
## GMM HMM - Ankur

## STS
## CNN
## LSTM
## Rawnet2


# Run augmented data and original data through pipelines and store results
