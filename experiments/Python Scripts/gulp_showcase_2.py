import pickle
import random
import numpy as np
import pandas as pd
import librosa
import librosa.display
import matplotlib.pyplot as plt
import seaborn as sns
from glob import glob
import IPython.display as ipd
from itertools import cycle
from tqdm import tqdm
from scipy.fft import fft, fftfreq
from scipy import signal

sr: int = 44100
def gulp_to_volume(num_gupls: int) -> float:
    return (random.randint(10, 50) * 0.1 * num_gupls);

def num_to_sec(num: int) -> float:
  return num / sr

audio_to_process: str = input("Please enter name of the audio you want to process: ")
print()

audio = librosa.load(audio_to_process, sr=sr)

from tqdm import tqdm
from scipy.fft import fft, fftfreq
from scipy import signal


frame_time = 16
frame_size = int(sr * frame_time / 1000)

overlap_per = 50
overlap_time = frame_time * overlap_per / 100
overlap = int(frame_size * overlap_per / 100)

print(f"Frame size: {frame_size}")
print(f"Frame time in milliseconds: {frame_time}ms")

print(f"Overlap time in milliseconds: {overlap_time}")
print(f"Overlap size: {overlap}")

# Statistical Features- time
time_frame_data_mins = []
time_frame_data_maxs = []
time_frame_data_means = []
time_frame_data_medians = []
time_frame_data_stds = []
time_frame_data_ptps = []

# Time domain features
amplitude_evelopes = []
root_mean_square_energies = []
zero_crossing_rates = []

# Statistical Features- freq
freq_magnitude_prod_frame_data_mins = []
freq_magnitude_prod_frame_data_maxs = []
freq_magnitude_prod_frame_data_means = []
freq_magnitude_prod_frame_data_medians = []
freq_magnitude_prod_frame_data_stds = []
freq_magnitude_prod_frame_data_ptps = []

# Frequency domain features
split_frequency = 1500
band_energy_ratios = []
spectral_centroids = []
bandwidths = []

# Gulp Data
gulp_constituencies = []

# Audio Metadata
audio_name = []

print(f'Processing audio signal file: {audio_to_process}')
y = audio
for start in tqdm(range(0, len(y), overlap)):

    # Getting Frame
    stop = (start + frame_size) if (start + frame_size) < len(y) else (len(y) - 1)
    time_frame_data = y[start:stop]
    if (stop - start) < overlap: break

    # Time based Statistical Features
    time_frame_data_mins.append(np.amin(time_frame_data))
    time_frame_data_maxs.append(np.amax(time_frame_data))
    time_frame_data_means.append(np.mean(time_frame_data))
    time_frame_data_medians.append(np.median(time_frame_data))
    time_frame_data_stds.append(np.std(time_frame_data))
    time_frame_data_ptps.append(np.ptp(time_frame_data))

    # Time domain features
    amplitude_evelopes.append(np.amax(time_frame_data))
    root_mean_square_energies.append(np.sqrt(np.mean(time_frame_data**2)))
    zero_crossing_rates.append(0.5 * np.sum([np.abs(np.sign(time_frame_data[idx]) - np.sign(time_frame_data[idx + 1])) for idx in range(len(time_frame_data) - 1)]))

    # Fourier transform with hann windowing
    N = (len(time_frame_data))
    yf = np.abs(np.fft.fft(signal.hann(N) * time_frame_data)) [:N//2]
    xf = np.linspace(0, sr, N)[:N//2]

    # Frequency based Statistical Features
    freq_magnitude_prod_frame_data = xf * yf
    freq_magnitude_prod_frame_data_mins.append(np.amin(freq_magnitude_prod_frame_data))
    freq_magnitude_prod_frame_data_maxs.append(np.amax(freq_magnitude_prod_frame_data))
    freq_magnitude_prod_frame_data_means.append(np.mean(freq_magnitude_prod_frame_data))
    freq_magnitude_prod_frame_data_medians.append(np.median(freq_magnitude_prod_frame_data))
    freq_magnitude_prod_frame_data_stds.append(np.std(freq_magnitude_prod_frame_data))
    freq_magnitude_prod_frame_data_ptps.append(np.ptp(freq_magnitude_prod_frame_data))

    # Frequency domain features
    band_energy_ratios.append(np.sum(yf[:int(split_frequency* (N / sr))] ** 2) / np.sum(yf[int(split_frequency* (N / sr)):] ** 2))
    spectral_centroids.append(np.sum(yf * xf) / np.sum(yf))
    bandwidths.append(np.sum((xf - (np.sum(yf * xf) / np.sum(yf))) * yf) / np.sum(yf))

    # Gulp Data
    gulp_constituencies.append(int(len(gulp_data.loc[(gulp_data.audio == audio_to_process) & (gulp_data.total_start_sec <= num_to_sec(start)) & (gulp_data.total_stop_sec >= num_to_sec(stop))]) > 0))

    # Audio Metadata
    audio_name.append(audio_signal)

# Dataframe initialization
gulp_analysis = pd.DataFrame()

# Statistical Features- time
gulp_analysis['time_frame_data_mins'] = time_frame_data_mins
gulp_analysis['time_frame_data_maxs'] = time_frame_data_maxs
gulp_analysis['time_frame_data_means'] = time_frame_data_means
gulp_analysis['time_frame_data_medians'] = time_frame_data_medians
gulp_analysis['time_frame_data_stds'] = time_frame_data_stds
gulp_analysis['time_frame_data_ptps'] = time_frame_data_ptps

# Time domain features
gulp_analysis['amplitude_evelopes'] = amplitude_evelopes
gulp_analysis['root_mean_square_energies'] = root_mean_square_energies
gulp_analysis['zero_crossing_rates'] = zero_crossing_rates

# Statistical Features- freq
gulp_analysis['freq_magnitude_prod_frame_data_mins'] = freq_magnitude_prod_frame_data_mins
gulp_analysis['freq_magnitude_prod_frame_data_maxs'] = freq_magnitude_prod_frame_data_maxs
gulp_analysis['freq_magnitude_prod_frame_data_means'] = freq_magnitude_prod_frame_data_means
gulp_analysis['freq_magnitude_prod_frame_data_medians'] = freq_magnitude_prod_frame_data_medians
gulp_analysis['freq_magnitude_prod_frame_data_stds'] = freq_magnitude_prod_frame_data_stds
gulp_analysis['freq_magnitude_prod_frame_data_ptps'] = freq_magnitude_prod_frame_data_ptps

# Frequency domain features
gulp_analysis['band_energy_ratios'] = band_energy_ratios
gulp_analysis['spectral_centroids'] = spectral_centroids
gulp_analysis['bandwidths'] = bandwidths

# Gulp Data
gulp_analysis['gulp_constituencies'] = gulp_constituencies

# Audio Metadata
gulp_analysis['audio_name'] = audio_name

clf = pickle.load(open('gulp_detection_model.sav', 'wb'))
