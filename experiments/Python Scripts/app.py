import base64
import os
import time
import boto3
import pickle
import random
import numpy as np
import pandas as pd
import librosa
from glob import glob
from itertools import cycle
from tqdm import tqdm
from scipy.fft import fft, fftfreq
from scipy import signal
from sklearn.svm import SVR
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from scipy.fft import fft, fftfreq
from scipy import signal

from joblib import Parallel, delayed
from frame_processing import process_frame
import time
import sys

import threading
import concurrent.futures
import matplotlib.pyplot as plt
import sendgrid
import os
from sendgrid.helpers.mail import *

sg = sendgrid.SendGridAPIClient(api_key='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')
# sg = sendgrid.SendGridAPIClient(os.environ.get('SENDGRID_API_KEY'))
from_email = ("accounts@remotesymphony.com")
# from_email = "kapil.chauhan@remotesymphony.com"
#To('kapil.chauhan@remotesymphony.com', 'Kapil Chauhan', p=1),
to_emails = [
        To('kapil.chauhan@remotesymphony.com', 'Kapil Chauhan', p=1),
        To('sameer.khadilkar@remotesymphony.com', 'Sameer Khadilkar', p=1),
        To('ankur.patil@remotesymphony.com', 'Ankur Patil', p=1),
        To('khilan.shine@remotesymphony.com', 'Khilan Shine', p=1),
        To('vaishali.nadgonde@remotesymphony.com', 'Vaishali Nadgonde', p=1),
        To('deepak.jha@remotesymphony.com', 'Deepak Jha', p=1),
        To('aayush.rai@remotesymphony.com', 'Aayush Rai', p=1),
        To('surayya.vk@gadgeon.com', 'Surayya VK', p=1),
        To('akhila.kala@gadgeon.com', 'Akhila Kala', p=1),
    ]

from flask import Flask, request, jsonify
app = Flask(__name__)


sr: int = 44100


frame_time = 800
frame_size = int(sr * frame_time / 1000)

overlap_per = 50
overlap_time = frame_time * overlap_per / 100
overlap = int(frame_size * overlap_per / 100)

def twos_complement(hexstr,bits):
    value = int(hexstr,16)
    if value & (1 << (bits-1)):
        value -= 1 << bits
    return value

def binary_data_loading(file):
    if file == '': return []
    hex_data = open(file, 'rb').read().hex()
    final_data = []
    for base_idx in range(0, len(hex_data), 28):
        row = {
                'hex_str_sec': '',
                'second_epoch_int': 0,
                'second_epoch_twos_complement': 0,
                'hex_str_ms' : '',
                'ms_epoch_int': 0,
                'ms_epoch_twos_complement': 0,
                'x_val': 0,
                'y_val': 0,
                'z_val': 0
                }

        idx = base_idx
        row['hex_str_sec'] = hex_data[idx + 6: idx + 8] + hex_data[idx + 4: idx + 6] + hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]
        row['second_epoch_int'] = int((hex_data[idx + 6: idx + 8] + hex_data[idx + 4: idx + 6] + 
                                hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]), 16)
        row['second_epoch_twos_complement'] = twos_complement((hex_data[idx + 6: idx + 8] + hex_data[idx + 4: idx + 6] + 
                                hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]), 32)

        idx += 8
        row['hex_str_ms'] = hex_data[idx + 6: idx + 8] + hex_data[idx + 4: idx + 6] + hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]
        row['ms_epoch_int'] = int((hex_data[idx + 6: idx + 8] + hex_data[idx + 4: idx + 6] + 
                                hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]), 16)
        row['ms_epoch_twos_complement'] = twos_complement((hex_data[idx + 6: idx + 8] + hex_data[idx + 4: idx + 6] + 
                                hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]), 32)
        
        idx += 8
        row['x_val'] = twos_complement((hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]), 16)

        idx += 4
        row['y_val'] = twos_complement((hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]), 16)

        idx += 4
        row['z_val'] = twos_complement((hex_data[idx + 2: idx + 4] + hex_data[idx: idx + 2]), 16)

        final_data.append(row)

    return final_data



def gulp_to_volume(num_gupls: int) -> float:
    return (random.randint(25, 35) * num_gupls);


def num_to_sec(num: int) -> float:
  return num / sr

def get_num_gulps(num: float, audio_name: str, audio_len: int, sr: int) -> int:
    # return np.round(random.randint(25, 35) * (audio_len / sr) * 0.001)
    return np.round(num * 0.01)


def process_audio(file_to_download, audio, sr):

    audio_name = []

    y = audio
    results = Parallel(n_jobs=-1)(
            delayed(process_frame)(start, y, file_to_download, sr) for start in range(0, len(y), overlap)
        )

    combined_results = {
        'time_frame_data_mins': [],
        'time_frame_data_maxs': [],
        'time_frame_data_means': [],
        'time_frame_data_medians': [],
        'time_frame_data_stds': [],
        'time_frame_data_ptps': [],
        'amplitude_evelopes': [],
        'root_mean_square_energies': [],
        'zero_crossing_rates': [],
        'freq_magnitude_prod_frame_data_mins': [],
        'freq_magnitude_prod_frame_data_maxs': [],
        'freq_magnitude_prod_frame_data_means': [],
        'freq_magnitude_prod_frame_data_medians': [],
        'freq_magnitude_prod_frame_data_stds': [],
        'freq_magnitude_prod_frame_data_ptps': [],
        'band_energy_ratios': [],
        'spectral_centroids': [],
        'bandwidths': [],
        'audio_name': [],
        }

    for result in results:
        for key in result.keys():
            if len(result[key]) == 0: continue
            combined_results[key].extend(result[key])


    gulp_analysis = pd.DataFrame()

    for key in combined_results.keys():
        gulp_analysis[key] = combined_results[key]


    x = gulp_analysis.drop(['audio_name'], axis=1).dropna().values

    clf = pickle.load(open('gulp_detection_model_2.sav', 'rb'))

    num_of_gulps: int = get_num_gulps(sum(clf.predict(x)), file_to_download, len(audio), sr)
    volume_of_liquid_consumed: float = gulp_to_volume(num_of_gulps)

    return {"Volume": (round(volume_of_liquid_consumed, 2)), "Gulps": num_of_gulps}


session = boto3.Session(
    aws_access_key_id="AKIAYYAW5LBDB2ILZBUV",
    aws_secret_access_key="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",
)

def time_stamp_int(dataframe):
    t_sec = dataframe['second_epoch_int']
    t_micros = dataframe['ms_epoch_int']
    dataframe['time'] = t_sec + t_micros/1000000
    
    return dataframe

def time_stamp_twos(dataframe):
    t_sec = dataframe['second_epoch_twos_complement']
    t_micros = dataframe['ms_epoch_twos_complement']
    dataframe['time'] = t_sec + t_micros/1000000
    
    return dataframe

@app.route('/', methods=['POST'])
def receive_data():
    acc_avail = False
    audio_avail = False

    file_to_download = request.get_json()
    print(file_to_download)

    s3 = session.resource("s3")

    bucket = s3.Bucket('bib-poc1')
    try:
        if file_to_download not in os.listdir():
            bucket.download_file(file_to_download, f'./{file_to_download}')
        audio, sr = librosa.load(f'./{file_to_download}', sr=44100)
        audio_avail = True
    except:
        pass

    try:
        acc_file = file_to_download.split('SDR')[0] + "ACL_1.bin"
        if acc_file not in os.listdir():
            print(f"Downloading: {acc_file}")
            bucket.download_file(acc_file, f'./{acc_file}')
            print('file downloaded')

        ab = binary_data_loading(acc_file)
        abc = pd.DataFrame(ab)
        acc_avail = True
    except Exception as e:
        print(str(e))

    if acc_avail:
        # make up some data
        x = np.linspace(
                0, # start
                (abc.second_epoch_int.max() - abc.second_epoch_int.min()),
                num = len(abc)
            ) 

        sample_rate = len(abc) / (abc.second_epoch_int.max() - abc.second_epoch_int.min())
        abc['adjusted_x'] = abc.x_val.values
        abc.loc[abc.adjusted_x == 0, "adjusted_x"] = abc['adjusted_x'].mean()
        a = abc["adjusted_x"]

        abc['adjusted_y'] = abc.y_val.values
        abc.loc[abc.adjusted_y == 0, "adjusted_y"] = abc['adjusted_y'].mean()
        b = abc["adjusted_y"]

        abc['adjusted_z'] = abc.z_val.values
        abc.loc[abc.adjusted_z == 0, "adjusted_z"] = abc['adjusted_z'].mean()
        c = abc["adjusted_z"]

        fig, ax = plt.subplots(3)
        ax[0].plot(x, a)
        ax[1].plot(x, b)
        ax[2].plot(x, c)
        plt.savefig(acc_file.split('.')[0]+'.png')
        plt.clf()

    if audio_avail:
        x_audio = np.linspace(
                0, # start
                len(audio) / sr,
                num = len(audio)
            )

        plt.plot(x_audio, audio)
        plt.savefig(file_to_download.split('.')[0]+'.png')
        plt.clf()


    print("file loaded")
    start_time = time.time()
    got_result = False

    try:
        if audio_avail:
            with concurrent.futures.ThreadPoolExecutor(max_workers=None) as executor:
                future = executor.submit(process_audio, file_to_download, audio, sr)
                result = future.result()

            print(result)
            got_result = True

            if acc_avail:
                content = Content("text/plain", f"Length of accelerometer file: {round(abc.second_epoch_int.max() - abc.second_epoch_int.min())} seconds, length of sound file: {round(len(audio) / sr)} seconds, number of gulps: {result['Gulps']}")
            else:
                content = Content("text/plain", f"Length of sound file: {round(len(audio) / sr)} seconds, number of gulps: {result['Gulps']}")
        else:
            if acc_avail:
                content = Content("text/plain", f"Length of accelerometer file: {round(abc.second_epoch_int.max() - abc.second_epoch_int.min())} seconds")
            else:
                content = Content("text/plain", f"No Files Found")
    except:
        if acc_avail:
            content = Content("text/plain", f"Length of accelerometer file: {round(abc.second_epoch_int.max() - abc.second_epoch_int.min())} seconds, length of sound file: {round(len(audio) / sr)} seconds")
        else:
            content = Content("text/plain", f"No Files Found")


    elapsed_time = time.time() - start_time

    print(f"Elapsed time: {elapsed_time:.2f} seconds")
    subject = "Accelerometer and sound file for " + file_to_download.split('SDR')[0]
    mail = Mail(from_email, to_emails, subject, content)
    if acc_avail:
        with open(acc_file.split('.')[0]+'.png', 'rb') as f:
            data = f.read()
        mail.attachment = sendgrid.Attachment(
            disposition='inline',
            file_name='accelerometer'+acc_file.split('.')[0]+'.png',
            file_type='image/png',
            file_content=base64.b64encode(data).decode(),
            content_id='accelerometer_graph',
        )
    if audio_avail:
        with open(file_to_download.split('.')[0]+'.png', 'rb') as f:
            audio_data = f.read()
        mail.attachment = sendgrid.Attachment(
            disposition='inline',
            file_name='audio_'+file_to_download.split('.')[0]+'.png',
            file_type='image/png',
            file_content=base64.b64encode(audio_data).decode(),
            content_id='audio_graph',
        )

    try:
        response = sg.client.mail.send.post(request_body=mail.get())
        print(response.status_code)
        print(response.body)
        print(response.headers)
    except Exception as e:
        print(str(e))

    if got_result:
        return {"Volume": (result['Volume']), "Gulps": result['Gulps']}, 201
    else:
        return {"Volume": 0, "Gulps": 0}, 201


if __name__ == '__main__':
    app.run(debug=True)

