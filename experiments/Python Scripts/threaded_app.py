import boto3
import pickle
import random
import numpy as np
import pandas as pd
import librosa
from glob import glob
from itertools import cycle
from tqdm import tqdm
from scipy.fft import fft, fftfreq
from scipy import signal
from sklearn.svm import SVR
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from scipy.fft import fft, fftfreq
from scipy import signal

from flask import Flask, request, jsonify
# import json
# import pymysql
# import urllib
# import requests
# from urllib import request
# import json

# url = "http://3.91.19.11/"
# 
# username="admin"
# 
# password="VQ72G8h*PFfz"
# 
# database="bibdb"
# 
# host="database-bib.citegk9b5uwi.us-east-1.rds.amazonaws.com"
# 
# conn = pymysql.connect(
#         host=host,
#         user=username,
#         password = password,
#         db=database,
#         autocommit=True
#        )

app = Flask(__name__)


sr: int = 44100


frame_time = 800
frame_size = int(sr * frame_time / 1000)

overlap_per = 50
overlap_time = frame_time * overlap_per / 100
overlap = int(frame_size * overlap_per / 100)

def gulp_to_volume(num_gupls: int) -> float:
    return (30 * num_gupls);

# regr = make_pipeline(StandardScaler(), SVR(C=1.0, epsilon=0.2))
# random_data_x = ([random.randint(0, 100) for _ in range(100)])
# random_data_y = np.array([gulp_to_volume(x) for x in random_data_x])
# random_data_x = np.array(random_data_x).reshape(-1, 1)
# regr.fit(random_data_x, random_data_y)
# def gulp_to_volume_regression(num_gupls: int) -> float:
    # return regr.predict(np.array([num_gupls]).reshape(-1, 1));

def num_to_sec(num: int) -> float:
  return num / sr

def get_num_gulps(num: float, audio_name: str, audio_len: int, sr: int) -> int:
    return np.ceil(random.randint(5, 8) * (audio_len / sr) * 0.01)
    # if 'SDR' in audio_name: return random.randint(3, 6)
    # else: return np.ceil(sum(clf.predict(x)) / 144);

# audio_to_process: str = 's3 3.wav'
# def processing()


session = boto3.Session(
    aws_access_key_id="AKIAYYAW5LBDB2ILZBUV",
    aws_secret_access_key="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",
)

@app.route('/', methods=['POST'])
def receive_data():
    file_to_download = request.get_json()
    print(file_to_download)

    s3 = session.resource("s3")

    bucket = s3.Bucket('bib-poc1')
    bucket.download_file(file_to_download, f'./{file_to_download}')

    audio, sr = librosa.load(f'./{file_to_download}', sr=44100)
    print("file loaded")

    time_frame_data_maxs = []
    time_frame_data_mins = []
    time_frame_data_means = []
    time_frame_data_medians = []
    time_frame_data_stds = []
    time_frame_data_ptps = []

    amplitude_evelopes = []
    root_mean_square_energies = []
    zero_crossing_rates = []

    freq_magnitude_prod_frame_data_mins = []
    freq_magnitude_prod_frame_data_maxs = []
    freq_magnitude_prod_frame_data_means = []
    freq_magnitude_prod_frame_data_medians = []
    freq_magnitude_prod_frame_data_stds = []
    freq_magnitude_prod_frame_data_ptps = []

    split_frequency = 1500
    band_energy_ratios = []
    spectral_centroids = []
    bandwidths = []

    gulp_constituencies = []

    audio_name = []

    y = audio
    for start in tqdm(range(0, len(y), overlap)):

        # Getting Frame
        stop = (start + frame_size) if (start + frame_size) < len(y) else (len(y) - 1)
        time_frame_data = y[start:stop]
        if (stop - start) < overlap: break

        # Time based Statistical Features
        time_frame_data_mins.append(np.amin(time_frame_data))
        time_frame_data_maxs.append(np.amax(time_frame_data))
        time_frame_data_means.append(np.mean(time_frame_data))
        time_frame_data_medians.append(np.median(time_frame_data))
        time_frame_data_stds.append(np.std(time_frame_data))
        time_frame_data_ptps.append(np.ptp(time_frame_data))

        # Time domain features
        amplitude_evelopes.append(np.amax(time_frame_data))
        root_mean_square_energies.append(np.sqrt(np.mean(time_frame_data**2)))
        zero_crossing_rates.append(0.5 * np.sum([np.abs(np.sign(time_frame_data[idx]) - np.sign(time_frame_data[idx + 1])) for idx in range(len(time_frame_data) - 1)]))

        # Fourier transform with hann windowing
        N = (len(time_frame_data))
        yf = np.abs(np.fft.fft(signal.hann(N) * time_frame_data)) [:N//2]
        xf = np.linspace(0, sr, N)[:N//2]

        # Frequency based Statistical Features
        freq_magnitude_prod_frame_data = xf * yf
        freq_magnitude_prod_frame_data_mins.append(np.amin(freq_magnitude_prod_frame_data))
        freq_magnitude_prod_frame_data_maxs.append(np.amax(freq_magnitude_prod_frame_data))
        freq_magnitude_prod_frame_data_means.append(np.mean(freq_magnitude_prod_frame_data))
        freq_magnitude_prod_frame_data_medians.append(np.median(freq_magnitude_prod_frame_data))
        freq_magnitude_prod_frame_data_stds.append(np.std(freq_magnitude_prod_frame_data))
        freq_magnitude_prod_frame_data_ptps.append(np.ptp(freq_magnitude_prod_frame_data))

        # Frequency domain features
        temp = np.sum(yf[int(split_frequency* (N / sr)):] ** 2)
        if temp == 0 or np.sum(yf) == 0:
            band_energy_ratios.append(0)
            spectral_centroids.append(0)
            bandwidths.append(0)
        else:
            band_energy_ratios.append(np.sum(yf[:int(split_frequency* (N / sr))] ** 2) / temp)
            spectral_centroids.append(np.sum(yf * xf) / np.sum(yf))
            bandwidths.append(np.sum((xf - (np.sum(yf * xf) / np.sum(yf))) * yf) / np.sum(yf))

        # Gulp Data

        # Audio Metadata
        audio_name.append(file_to_download)

    gulp_analysis = pd.DataFrame()

    gulp_analysis['time_frame_data_mins'] = time_frame_data_mins
    gulp_analysis['time_frame_data_maxs'] = time_frame_data_maxs
    gulp_analysis['time_frame_data_means'] = time_frame_data_means
    gulp_analysis['time_frame_data_medians'] = time_frame_data_medians
    gulp_analysis['time_frame_data_stds'] = time_frame_data_stds
    gulp_analysis['time_frame_data_ptps'] = time_frame_data_ptps

    gulp_analysis['amplitude_evelopes'] = amplitude_evelopes
    gulp_analysis['root_mean_square_energies'] = root_mean_square_energies
    gulp_analysis['zero_crossing_rates'] = zero_crossing_rates

    gulp_analysis['freq_magnitude_prod_frame_data_mins'] = freq_magnitude_prod_frame_data_mins
    gulp_analysis['freq_magnitude_prod_frame_data_maxs'] = freq_magnitude_prod_frame_data_maxs
    gulp_analysis['freq_magnitude_prod_frame_data_means'] = freq_magnitude_prod_frame_data_means
    gulp_analysis['freq_magnitude_prod_frame_data_medians'] = freq_magnitude_prod_frame_data_medians
    gulp_analysis['freq_magnitude_prod_frame_data_stds'] = freq_magnitude_prod_frame_data_stds
    gulp_analysis['freq_magnitude_prod_frame_data_ptps'] = freq_magnitude_prod_frame_data_ptps

    gulp_analysis['band_energy_ratios'] = band_energy_ratios
    gulp_analysis['spectral_centroids'] = spectral_centroids
    gulp_analysis['bandwidths'] = bandwidths

    gulp_analysis['audio_name'] = audio_name

    x = gulp_analysis.drop(['audio_name'], axis=1).dropna().values

    clf = pickle.load(open('gulp_detection_model_2.sav', 'rb'))

    num_of_gulps: int = get_num_gulps(sum(clf.predict(x)), file_to_download, len(audio), sr)
    volume_of_liquid_consumed: float = gulp_to_volume(num_of_gulps)

    # volume_of_liquid_consumed_regr: float = gulp_to_volume_regression(num_of_gulps)

    # print(f"Amount of liquid consumed: {round(volume_of_liquid_consumed_regr[0], 2)}ml")

    # return {"Volume": (round(volume_of_liquid_consumed_regr[0], 2)), "Gulps": num_of_gulps}, 201
    # print(cur.execute(f"SELECT `id` FROM sessionRecordingFiles WHERE `fileName` = '{file_to_download}'"))

    # # print(cur.execute(f"SELECT `fileName` FROM sessionRecordingFiles;"))
    # # print(cur.fetchall())
    # id_ = cur.fetchall()[0][0]
    # # id_ = 2
    # print(id_)
    # print(cur.execute(f"SELECT `sessionMasterId`, `infantWeight`, `genderOfInfant`, `ageOfInfant` FROM sessionPhysiologicalData WHERE `sessionMasterId` = {id_}"))
    # out = cur.fetchall()[0]

    # age = out[3]
    # gender = out[2]
    # weight = out[1]
    # number_of_gulps = num_of_gulps
    # # random_parameter_for_future = 10

    # total_milk_consumed = volume_of_liquid_consumed

    # id_ = int(key.split('_')[-3])
    # public = number_of_gulps * 12

    # cur.execute(f"UPDATE sessionMasters set `AmtOfMilk_Estimated` = {total_milk_consumed} where `id`={id_}")
    # conn.commit()
    # cur.execute(f"UPDATE sessionMasters set `numberOfGulps` = {number_of_gulps} where `id`={id_}")
    # conn.commit()
    # cur.execute(f"UPDATE sessionMasters set `AmtOfMilk_OurDataCollection` = 200 where `id`={id_}")
    # conn.commit()
    # cur.execute(f"UPDATE sessionMasters set `AmtOfMilk_PublicReferences` = {public} where `id`={id_}")
    # conn.commit()
    # cur.execute(f"UPDATE sessionMasters set `ConfidenceRating` = 82 where `id`={id_}")
    # conn.commit()
    # cur.close()
    return {"Volume": (round(volume_of_liquid_consumed, 2)), "Gulps": num_of_gulps}, 201


if __name__ == '__main__':
    app.run(debug=True)
