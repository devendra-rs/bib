''' This file consists of various common functions required to run the various architectures.'''

# Importing required libraries 

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
import pandas as pd
from scipy import stats
import soundfile as sf 
import librosa
import math
import os
import sklearn as sk
from sklearn.mixture import GaussianMixture


# read .csv file containing time stamps and converts into array
def convert_to_seconds(filename):
  fname = filename
  a = pd.read_csv(fname)
  b = len(a)
  st = []
  sp = []
  for i in range(len(a)):
    st1 = a['start'][i]
    a1, a2, a3 = st1.split('.')
    st_s = 60*int(a1) + int(a2) + int(a3)*(0.1**(len(a3)))
    st.append(st_s)
    sp1 = a['stop'][i]
    b1, b2, b3 = sp1.split('.')
    sp_s = 60*int(b1) + int(b2) + int(b3)*(0.1**(len(b3)))
    sp.append(sp_s)

  return np.c_[np.array(st), np.array(sp)]
  

# It extracts MFCC feature sets for each gulp segment and saves it in suggested location  
def save_gulp_feats(sound_file, st_sp_file, save_folder):
  sig_seg = []
  s,fs = librosa.load(sound_file, sr = None, mono = True)
  ab = convert_to_seconds(st_sp_file)
  for i, (st, sp) in enumerate(ab):
    t1 = range(math.floor(st * fs),  math.floor(sp * fs))
    sig_seg = s[t1]
    
    stat = librosa.feature.mfcc(sig_seg, sr = fs, n_mfcc = 20, n_mels = 40)  # win_length = 960, hop_length = 480
    d_feat = librosa.feature.delta(stat, width = 3)
    dd_feat = librosa.feature.delta(d_feat, width = 3)

    feat = np.concatenate((stat, d_feat, dd_feat), axis = 0)

    a = sound_file.split('/')[-1][:-4]
    file_name = save_folder + a + '_gulp_' + str(i)
    np.save(file_name, feat, allow_pickle=False)


# It extracts MFCC feature sets for each non-gulp segment and saves it in suggested location     
def save_nongulp_feats_except_first(sound_file, st_sp_file, save_folder):
  sig_seg = []
  s,fs = librosa.load(sound_file, sr = None, mono = True)
  ab = convert_to_seconds(st_sp_file)
  for i, (st1, sp1) in enumerate(ab):
    if i < len(ab) - 1:
      j = i + 1
      st2, sp2 = ab[j]
      t1 = range(math.floor(sp1 * fs), math.floor(st2 * fs))
      sig_seg = s[t1]
      stat = librosa.feature.mfcc(sig_seg, sr = fs, n_mfcc = 20, n_mels = 40)
      d_feat = librosa.feature.delta(stat, width = 3)
      dd_feat = librosa.feature.delta(d_feat, width = 3)
      feat = np.concatenate((stat, d_feat, dd_feat), axis = 0)
      a = sound_file.split('/')[-1][:-4]
      file_name = save_folder + a + '_nongulp_' + str(i)
      np.save(file_name, feat, allow_pickle=False)


# This function helps to load the saved feature sets    
def load_saved_feats(folder_path):
  feats_list = []
  for file in os.listdir(folder_path):
    filepath = os.path.join(folder_path, file)
    feat = np.load(filepath)
    feats_list.append(feat.T)
  feats_array = np.vstack(feats_list)
  return feats_array

    